﻿using System;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using System.Linq;
using System.Threading.Tasks;

public class DbContext {

    private static DbContext _instance = null;
    private readonly static object _syncObj = new object();

    private IDbConnection _connection;

    public static string Path { get; set; }
    public static DbContext GetInstance(string appPath)
    {
        if (_instance == null)
        {
            lock (_syncObj)
            {
                _instance = new DbContext(appPath);
            }
        }
        return _instance;                
    }

    public DbContext(string appPath)
    {
        OpenConnection(appPath);
    }

    private void OpenConnection(string appPath)
    {
        var path = "URI=file:" + appPath + "/DB/db.db";
        _connection = new SqliteConnection(path);
        _connection.Open();
    }

    private void CloseConnection()
    {
        _connection.Close();
        _connection = null;
    }

    public Task<List<T>> SelectQueryAsync<T>(string query) where T: class, new()
    {
        var result = new List<T>();
        return Task.Factory.StartNew(() =>
        {
            var command = _connection.CreateCommand();
            command.CommandText = query;
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var model = new T();
                var fields = typeof(T).GetProperties();

                for (var i = 0; i < reader.FieldCount; i++)
                {
                    var name = reader.GetName(i).ToUpper();
                    var field = fields.Where(n => n.Name.ToUpper() == name).FirstOrDefault();
                    var value = reader.GetValue(i);
                    field.SetValue(model, Convert.ChangeType(value, field.PropertyType), null);
                }
                result.Add(model);
            }
            reader.Close();
            reader = null;
            command.Dispose();
            command = null;
            return result;
        });
    }
}

