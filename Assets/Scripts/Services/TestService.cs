﻿using Assets.Scripts.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class TestService {

    private readonly DbContext _db;
    private const string _tableName = "Test";

    public TestService(DbContext db)
    {
        _db = db;
    }

    public async Task<List<TestModel>> GetItems()
    {
        return await _db.SelectQueryAsync<TestModel>("SELECT * FROM " + _tableName);
    }
}
