﻿namespace Assets.Scripts.Models
{
    public class TestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
