﻿using UnityEngine;

public class TestScript : AppMonoBehaviour
{
    // Use this for initialization
    public override async void Start () {
        base.Start();
        var _service = new TestService(DbContext);
        var items = await _service.GetItems();
        foreach (var item in items)
            Debug.Log("Name= " + item.Name + "  value =" + item.Value);
    }
}
