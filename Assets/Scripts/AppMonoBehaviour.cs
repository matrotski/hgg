﻿using UnityEngine;

public class AppMonoBehaviour : MonoBehaviour {

    public static DbContext DbContext;
    // Use this for initialization
    public virtual void Start () {
        if (DbContext == null)
            DbContext = DbContext.GetInstance(Application.dataPath);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
